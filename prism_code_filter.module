<?php
/**
 * @file
 * Text filter for highlighting PHP source code.
 *
 * This isn't really compatible with the prism module. This module is for adding
 * prism support to the codefilter module, which is too, not compatible with the
 * prism filter module.
 */

/**
 * Processes chunks of escaped PHP code into HTML.
 */
function prism_code_filter_process_php($text) {
  // Undo linebreak escaping.
  $text = str_replace('&#10;', "\n", $text);
  // Inline or block level piece?
  $multiline = strpos($text, "\n") !== FALSE;

  // Undo the escaping in the prepare step.
  $text = decode_entities($text);
  // Trim first leading line break.
  $text = preg_replace('@^[ \t]*[\n]@', '', $text);
  $text = prism_code_filter_fix_spaces($text);

  // Highlight as PHP.
  $text = '<code class="language-php">' . $text . '</code>';
  $text = $multiline ? '<pre class="codeblock">' . $text . '</pre>' : $text;

  return $text;
}

/**
 * Callback to replace content of the <?php ?> elements.
 */
function _prism_code_filter_process_php_callback($matches) {
  return prism_code_filter_process_php($matches[1]);
}

/**
 * Processes chunks of escaped code into HTML.
 */
function prism_code_filter_process_code($text, $attributes = '') {
  // Undo linebreak escaping.
  $text = str_replace('&#10;', "\n", $text);
  // Inline or block level piece?
  $multiline = strpos($text, "\n") !== FALSE;
  // Trim first leading line break.
  $text = preg_replace('@^[ \t]*[\n]@', '', $text);
  // Escape newlines.
  // $text = nl2br($text, FALSE);
  $text = decode_entities($text);

  // Add language class if one doesn't exist.
  if (strpos($attributes, 'class="') !== FALSE) {
    if (strpos($attributes, 'language-') === FALSE) {
      $attributes = str_replace('class="', 'class="language-php ', $attributes);
    }
  }
  else {
    $attributes .= ' class="language-php"';
  }

  // $text = str_replace(' ', '&nbsp;', $text);
  $text = prism_code_filter_fix_spaces($text);
  $text = '<code' . $attributes . '>' . $text . '</code>';
  $text = $multiline ? '<pre class="codeblock">' . $text . '</pre>' : $text;

  return $text;
}

/**
 * Callback to replace content of the <code> elements.
 *
 * @param array $matches
 *   An array of matches passed by preg_replace_callback().
 *
 * @return string
 *   A formatted string.
 */
function _prism_code_filter_process_code_callback($matches) {
  return prism_code_filter_process_code($matches[2], $matches[1]);
}

/**
 * Replace html space elements with literal space characters.
 *
 * @param string $text
 *   A string to fix spaces for.
 *
 * @return string
 *   A formatted string.
 */
function prism_code_filter_fix_spaces($text) {
  $text = preg_replace('@&nbsp;(?!&nbsp;)@', ' ', $text);
  // A single space before text is ignored by browsers. If a single space
  // follows a break tag, replace it with a non-breaking space.
  $text = preg_replace('@<br> ([^ ])@', '<br>&nbsp;$1', $text);
  return $text;
}

/**
 * Escape code blocks during input filter 'prepare'.
 *
 * @param string $text
 *   The string to escape.
 * @param string $type
 *   The type of code block, either 'code' or 'php'.
 * @param string $attributes
 *   (optional) The HTML attributes of the code block.
 *
 * @return string
 *   The escaped string.
 */
function prism_code_filter_escape($text, $type = 'code', $attributes = '') {
  // Note, pay attention to odd preg_replace-with-/e behaviour on slashes.
  // $text = str_replace('\"', '"', $text);
  $text = check_plain($text);

  // Protect newlines from line break converter.
  $text = str_replace(array("\r", "\n"), array('', '&#10;'), $text);

  // Prepare the class HTML attribute for the prism_code_filter tag.
  if (!empty($attributes)) {
    $attributes = ' ' . $attributes;
  }

  // Add prism_code_filter escape tags.
  $text = "[prism_code_filter_{$type}{$attributes}]{$text}[/prism_code_filter_$type]";

  return $text;
}

/**
 * Implements hook_filter_info().
 */
function prism_code_filter_filter_info() {
  $filters['prism_code_filter'] = array(
    'title' => t('Prism code filter'),
    'description' => t('Allows users to post code verbatim using &lt;code&gt; and &lt;?php ?&gt; tags.'),
    'prepare callback' => '_prism_code_filter_prepare',
    'process callback' => '_prism_code_filter_process',
    'settings callback' => '_prism_code_filter_settings',
    'default settings' => array(
      'nowrap_expand' => FALSE,
    ),
    'tips callback' => '_prism_code_filter_tips',
  );
  return $filters;
}

/**
 * Implements hook_filter_FILTER_prepare().
 *
 * Escape code tags to prevent other filters from acting on them.
 * <code> </code>, <?php ?>, [?php ?], <% %>, and [% %] tags are escaped.
 */
function _prism_code_filter_prepare($text, $format) {
  $text = preg_replace_callback('@<code([^>]*)>(.+?)</code>@s', '_prism_code_filter_escape_code_tag_callback', $text);
  $text = preg_replace_callback('@[\[<](\?php)(.+?)(\?)[\]>]@s', '_prism_code_filter_escape_php_tag_callback', $text);
  return $text;
}

/**
 * Callback to escape content of <code> elements.
 */
function _prism_code_filter_escape_code_tag_callback($matches) {
  // Extract a potentially existing class HTML attribute.
  $attributes = '';
  if (!empty($matches[1]) && preg_match('@class="[^"]+"@', $matches[1], $class_matches)) {
    $attributes = $class_matches[0];
  }

  return prism_code_filter_escape($matches[2], 'code', $attributes);
}

/**
 * Callback to escape content of <?php ?>, [?php ?], <% %>, and [% %] elements.
 */
function _prism_code_filter_escape_php_tag_callback($matches) {
  return prism_code_filter_escape($matches[2], 'php');
}

/**
 * Implements hook_filter_FILTER_process().
 */
function _prism_code_filter_process($text, $format) {
  $text = preg_replace_callback('@\[prism_code_filter_code( [^\]]+)?\](.+?)\[/prism_code_filter_code\]@s', '_prism_code_filter_process_code_callback', $text);
  $text = preg_replace_callback('@\[prism_code_filter_php\](.+?)\[/prism_code_filter_php\]@s', '_prism_code_filter_process_php_callback', $text);
  // A hack, so we can conditionally nowrap based on filter settings.
  // @todo Refactor how replacements are done so we can do this more cleanly.
  if ($format->settings['nowrap_expand']) {
    $text = str_replace('class="codeblock', 'class="codeblock nowrap-expand', $text);
  }
  return $text;
}

/**
 * Implements hook_filter_FILTER_tips().
 */
function _prism_code_filter_tips($format, $long = FALSE) {
  if ($long) {
    return t('To post pieces of code, surround them with &lt;code&gt;...&lt;/code&gt; tags. For PHP code, you can use &lt;?php ... ?&gt;, which will also colour it based on syntax.');
  }
  else {
    return t('You may post code using &lt;code&gt;...&lt;/code&gt; (generic) or &lt;?php ... ?&gt; (highlighted PHP) tags.');
  }
}

/**
 * Settings callback for the prism_code_filter filter.
 *
 * @see hook_filter_info()
 * @see hook_filter_FILTER_settings()
 */
function _prism_code_filter_settings($form, &$form_state, $filter, $format, $defaults) {
  $filter->settings += $defaults;

  $elements['nowrap_expand'] = array(
    '#type' => 'checkbox',
    '#title' => t('Expand code boxes on hover.'),
    '#description' => t('By default, code boxes inherit text wrapping from the active theme. With this setting, code boxes will not wrap, but will expand to full width on hover (with Javascript).'),
    '#default_value' => $filter->settings['nowrap_expand'],
  );

  return $elements;
}

/**
 * Implements hook_library().
 */
function prism_code_filter_library() {
  $location = prism_code_filter_return_library_location();

  $libraries['prism'] = array(
    'title' => 'Prism',
    'website' => 'http://prismjs.com',
    'version' => array(),
    'js' => array(
      $location . 'prism.js' => array(
        'scope' => 'footer',
      ),
    ),
    'css' => array(
      $location . 'prism.css' => array(
        'type' => 'file',
        'media' => 'screen',
      ),
    ),
  );

  return $libraries;
}

/**
 * Return appropriate library location.
 */
function prism_code_filter_return_library_location() {
  $file_base = '/libraries/prism/prism.js';

  // Check for a multisite installation.
  if (file_exists(conf_path() . $file_base)) {
    $location = conf_path() . '/libraries/prism/';
  }
  else {
    // Default.
    $location = 'sites/all/libraries/prism/';
  }

  return $location;
}

/**
 * Implements hook_init()
 */
function prism_code_filter_init() {
  drupal_add_library('prism_code_filter', 'prism');
}
